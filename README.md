 Simple usage

## Build progect

Run in console:

```
cargo build
```

Then yo can check for executable in `./target/debug`.
It is `./target/debug/guessing_game`.


## Run program

To run program you can use built executable (see previous section)
or you can run in console:

```
cargo run
```

## Content

+ `Rectangle` struct with `width` and `heigth` data and implements following funtionallity:
    * `area` - calculates area of current rectangle instance;
    * `can_hold` - checks if current rectangle can hold another one provided as parameter;
    * `::create_square` - assosiated (aka "static") function that creates instance of `Rectangle`
        with `width` equal `height`.
