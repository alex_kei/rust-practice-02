#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u64 {
        return u64::from(self.width) * u64::from(self.height);
    }

    fn can_hold(&self, other_rectangle: &Rectangle) -> bool {
        return (self.width > other_rectangle.width)
            && (self.height > other_rectangle.height);
    }

    fn create_square(width: u32) -> Self {
        Self {
            width,
            height: width,
        }
    }
}

fn main() {
    let rectangle = Rectangle {
        width: 12,
        height: 9,
    };

    println!(
        "{:?} has area {}",
        rectangle,
        rectangle.area(),
    );

    let scale: u32 = 2;
    let shape = Rectangle {
        width: dbg!(scale * 144),
        height: 96,
    };
    dbg!(&shape);

    println!(
        "{:?} {} hold {:?}",
        shape,
        if shape.can_hold(&rectangle) { "can" } else { "cannot" },
        rectangle,
    );
    println!(
        "{:?} {} hold {:?} ",
        rectangle,
        if rectangle.can_hold(&shape) { "can" } else { "cannot" },
        shape,
    );

    let square = Rectangle::create_square(128);
    println!(
        "This is a square - {:?}. It's area is {} pixels.",
        square,
        square.area(),
    );
}
